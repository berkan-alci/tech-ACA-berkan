# @Berkan Alci - Info tech screening ACA group:
# There are a few ways to create this. Back when I first started learning python, I created this game like it was a script
# My second idea was to create it with multiple classes (Player, Computer, Game). However nearing the end of this implementation I felt I was overusing classes
# It's never a good idea to use too many classes. Simple = more, so that's where I eventually went with the final implementation you see below:
import sys
from game import RPS
import pandas as pd  # Goal is short code, so I'd use pandas for table output
from tabulate import tabulate  # convert to a pretty table


def game_loop():
    print('Rock/Paper/Scissor game rules:\nRock vs paper -> paper wins\nRock vs scissors -> rock wins\nPaper vs scissors -> scissors wins')

    run = True
    game = RPS()
    while run:
        p_in = str(
            input("\nType 'rock', 'paper' or 'scissors' to play.\nType 'Q' to quit.\nChoice: ").lower())

        if p_in == "rock" or p_in == "paper" or p_in == "scissors":
            player, cpu, message = game.run_game(p_in)
            print(f"\nPlayer: {player} CPU: {cpu} -- WINNER: {message}")
            print(f"Player: {game.player_score} - CPU: {game.cpu_score}")
        elif p_in == "q":
            winner, history = game.game_winner()
            print(f"\n+==========+\n{winner}\n+==========+\n")

            out = []
            for game in history:
                # using tabulate & pd saves me about 10 lines & a for loop.
                output = tabulate(pd.DataFrame([game]),
                                  headers='keys', tablefmt='psql')
                out.append(output)
                print(output)

            # here we append the game history to the history text file
            with open('history.txt', 'a') as file_input:
                file_input.writelines(out)
                file_input.close()

            run = False
        else:
            print("Invalid input! Restart the game!")
            run = False


def main():
    # 2 args max: filename, arg -> [1] = history
    if len(sys.argv) == 2 and sys.argv[1] == '--history':
        # output of file to cli
        with open('history.txt', 'r') as file_output:
            content = file_output.read()
            file_output.close()
            print(content)
    elif len(sys.argv) == 1:
        game_loop()
    elif len(sys.argv) > 2:
        print('CORRECT USAGE: py ./main.py [--history]')
        sys.exit()


if __name__ == "__main__":
    # graceful shutdown
    try:
        main()
    except KeyboardInterrupt:
        sys.exit()
