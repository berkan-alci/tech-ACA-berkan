from random import randint


class RPS:
    def __init__(self):
        self.player_score = 0
        self.cpu_score = 0
        self._game_map = {0: "rock", 1: "paper", 2: "scissors"}
        # -1 = tie, 0 = r > s, 1 = p > r, 2 = s > p
        self._game_matrix = [[-1, 1, 0], [1, -1, 2], [0, 2, -1]]
        self._history = []  # list of dicts with active game hist

    # function that user should interact with to run the game
    def run_game(self, move):
        player_move, cpu_move, message = self._rule_validation(move)
        player = self._game_map[player_move]
        cpu = self._game_map[cpu_move]

        self._game_history(player, cpu, message)

        return player, cpu, message

    def _rule_validation(self, move):
        player_move = self._player_input(move)
        cpu_move = randint(0, 2)
        # determining winner based on matrix
        winner = self._game_matrix[player_move][cpu_move]
        message = ""

        if winner == player_move:
            self.player_score += 1
            message = "PLAYER"
        elif winner == cpu_move:
            self.cpu_score += 1
            message = "CPU"
        else:
            message = "TIE"

        return player_move, cpu_move, message

    def _player_input(self, move):
        player_move = 0
        # converting p_move to correct k:v pair
        match move:
            case "rock":
                player_move = 0
            case "paper":
                player_move = 1
            case "scissors":
                player_move = 2

        return player_move

    # After a game has run we take the user input, cpu move, current scores + winner
    def _game_history(self, player_move, cpu_move, message):
        current = {"player": player_move, "cpu": cpu_move,
                   "player_score": self.player_score, "cpu_score": self.cpu_score, "winner": message}
        copy = current.copy()
        self._history.append(copy)

    # when a game ends, this function is called to display the winner
    def game_winner(self):
        game_winner = ""
        if self.cpu_score > self.player_score:
            game_winner = "CPU won!"
        elif self.cpu_score < self.player_score:
            game_winner = "PLAYER won!"
        else:
            game_winner = "It's a TIE!"

        return game_winner, self._history
