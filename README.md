# Python - Rock Paper Scissors

## Assignment

Build a command line Rock/Paper/Scissors game.

The game should work as follows:

1. Show the game instructions on startup

> > > Rock/Paper/Scissor game rules:

Rock vs paper -> paper wins

Rock vs scissors -> rock wins

Paper vs scissors -> scissors wins

> > >

2. The user plays against the computer (random picked)

3. The user plays until he wants to stop (instructions how to stop the game should be provided)

4. When the user stops playing he gets an overview of the results in a table including who won the most games

5. The history of all played games over time is saved on disk

6. By adding the –history command line argument when executing the program the full game history including date/times when each game was played is shown (also table format) + who won the most games.

## Further instructions:

-   Make the code as short as possible
-   Code should be written according to [PEP 8](https://peps.python.org/pep-0008/)
-   Code should be properly documented

## Applicant information:

-   Clone git repo
-   `py ./main.py` to run & play game
-   `py ./main.py --history` to get the history data.
